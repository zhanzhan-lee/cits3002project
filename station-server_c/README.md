station data structure

kill a port in linux

sudo netstat -tuln | grep (port number) or sudo lsof -i :(port number)

sudo kill PID

struct station
    |-station name
    |-browser port
    |-query port
    |-station name from file
    |-x coordinate
    |-y coordinate
    |-number of neighbour
    |-[neighbours_port] ------- [localhost:4444, .....]
    |-[neighoburs] ------------ [neighbour 1]
                                    |-neighbour name
                                    |-port (reserved, have not got this yet)
                                    |-timetable[leaving hour][leaving min]
                                    |       |-leaving time
                                    |       |-arriving time
                                    |       |-travel time
                                    |       |-line name
                                    |       |-platform
                                    |       |-next neighbour
                                    |
                                [neighbour 2]
                                    |
                                [neighbour 3]

//  searching algorithm --------------------------------------------------------------------------------------------------
The A* search algorithm is a widely used algorithm in pathfinding and graph traversal. It efficiently finds the shortest path from a starting node to a goal node by considering both the cost of reaching a node from the start and a heuristic estimate of the cost to reach the goal from that node. Here's how it works:

    Initialization: The algorithm starts with an initial node, known as the start node, and a goal node. It initializes an empty set of open nodes and a set of closed nodes. The open set contains nodes that have been discovered but not yet evaluated, while the closed set contains nodes that have already been evaluated.

    Cost Calculation: For each node, the algorithm calculates two values:
        g(n)g(n): The cost of the cheapest path from the start node to node nn discovered so far.
        h(n)h(n): An estimate of the cost from node nn to the goal node. This is the heuristic function.

    Evaluation and Expansion: The algorithm iteratively selects nodes from the open set based on a combination of g(n)g(n) and h(n)h(n). It chooses the node with the lowest value of f(n)=g(n)+h(n)f(n)=g(n)+h(n) for evaluation. This node is removed from the open set and added to the closed set.

    Neighbor Expansion: The algorithm examines all neighboring nodes of the current node. For each neighbor, it calculates its f(n)f(n) value and evaluates whether it should be added to the open set for further consideration.

    Termination: The algorithm terminates when it either finds the goal node or when the open set is empty, indicating that there is no path from the start node to the goal node.

    Path Reconstruction: If the goal node is found, the algorithm reconstructs the shortest path from the start node to the goal node by backtracking through the parent pointers stored during the search.

A* is guaranteed to find the shortest path if the heuristic function satisfies certain conditions, such as being admissible (never overestimating the true cost to reach the goal) and consistent (satisfying the triangle inequality). When properly implemented with an appropriate heuristic function, A* is often more efficient than uninformed search algorithms like Dijkstra's algorithm because it can intelligently prioritize nodes that are likely to lead to the goal.

//  c select    --------------------------------------------------------------------------------------------------------------------------------
Certainly! select() is a system call in C that allows a program to monitor multiple file descriptors, waiting until one or more of the file descriptors become "ready" for some class of I/O operation (e.g., reading, writing, or error condition). It's commonly used in network programming for handling multiple network connections efficiently, but it can also be used for monitoring other types of file descriptors such as pipes, sockets, or regular files.

Here's how select() works in C:

    Preparing File Descriptors: Before calling select(), you need to prepare sets of file descriptors that you are interested in monitoring for read, write, or error conditions. These sets are represented by fd_set data structures. You can use FD_ZERO() to initialize an empty set, FD_SET() to add a file descriptor to a set, and FD_ISSET() to check if a file descriptor is part of a set.

    Setting Timeout: Optionally, you can set a timeout value to specify how long select() should wait for any file descriptors to become ready. If you don't want to specify a timeout, you can pass NULL as the timeout parameter to make select() wait indefinitely until at least one file descriptor becomes ready.

    Calling select(): Once you have prepared the file descriptor sets and optionally set the timeout, you call the select() function with the maximum file descriptor value plus one, and the sets of file descriptors you want to monitor for read, write, and error conditions.

    Checking for Ready File Descriptors: After select() returns, you can use FD_ISSET() to check which file descriptors are ready for the specified I/O operation (read, write, or error).

