#include "check.h"

void check_alloc(void *pointer)
{
    if (pointer == NULL) {
        perror("error allocate memory");
        exit(EXIT_FAILURE);
    }
}

void check_file_open(FILE *fp)
{
    if (fp == NULL) {
        perror("error open file");
        exit(EXIT_FAILURE);
    }
}

void check(int value, char *function)
{
    if (value < 0) {
        perror(function);
        exit(EXIT_FAILURE);
    }
}
