#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdio.h>
#include <stdlib.h>

extern void check_alloc(void *pointer);

extern void check_file_open(FILE *fp);

extern void check(int value, char *function);

#endif /* MESSAGE_H */
