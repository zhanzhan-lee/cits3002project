//  Original Author: Chris McDonald
//  linked list from cits2002 system programming

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "timetable.h"
#include "../check/check.h"

LIST *list_init()
{
    return NULL;
}

bool list_find(LIST *neighbours, char *neighbour_name) 
{
    while (neighbours != NULL) {
        if (strcmp(neighbours->neighbour_name, neighbour_name) == 0) {
            return true;
        }
        neighbours = neighbours->next_neighbour;
    }
    return false;
}

LIST *list_new_item(char *neighbour_name)
{
    LIST *new               = calloc(1, sizeof(LIST));
    check_alloc(new);
    new->neighbour_name     = strdup(neighbour_name);
    check_alloc(new->neighbour_name);
    new->port               = NULL;
    new->next_neighbour     = NULL;
    new->timetable          = timetable_init();
    return new;
}

LIST *list_add(LIST *neighbours, char *neighbour_name, int leaving_hour, int leaving_min,
               int arriving_hour, int arriving_min, char *platform, char *line_name, char *next_station)
{
    LIST *temp = neighbours;
    while (temp != NULL) {
        if (strcmp(temp->neighbour_name, neighbour_name) == 0) {
            timetable_add(temp->timetable, leaving_hour, leaving_min, arriving_hour, arriving_min, platform, line_name, next_station);
            return neighbours;
        }
        temp = temp->next_neighbour;
    }

    LIST *new = list_new_item(neighbour_name);

    new->timetable = timetable_init();
    timetable_add(new->timetable, leaving_hour, leaving_min, arriving_hour, arriving_min, platform, line_name, next_station);
    new->next_neighbour = neighbours;
    return new;
}

char *list_get_port(LIST *neighbours, char *neighbour_name)
{
    while (neighbours != NULL) {
        if (strcmp(neighbours->neighbour_name, neighbour_name) == 0) {
            return neighbours->port;
        }
        neighbours = neighbours->next_neighbour;
    }
    return NULL;
}

SHIFT *list_get_next_shift(LIST *neighbours, char *neighbour_name, int leaving_hour, int leaving_min) 
{
    while (neighbours != NULL) {
        if (strcmp(neighbours->neighbour_name, neighbour_name) == 0) {
            SHIFT *op;
            op = timetable_find_next(neighbours->timetable, leaving_hour, leaving_min);
            return op;
        }
        neighbours = neighbours->next_neighbour;
    }
    return NULL;
}

void list_print_timetable(LIST *neighbours)
{
    LIST *temp = neighbours;
    while (temp != NULL) {
        printf("Neighbour: %s\n", temp->neighbour_name);
        // for (int h = 0; h < 24; h++) {
        //     for (int m = 0; m < 60; m++) { 
        //         SHIFT *shift = (*temp->timetable)[h][m];
        //         if (shift != NULL) {
        //             printf("Destination: %s\t Leaving Time: %02d:%02d\t Line: %s\t Platform: %s\t Arriving Time: %02d:%02d\t Travel Time: %d mins\n",
        //                    temp->neighbour_name, shift->leaving_hour, shift->leaving_min, shift->line_name, shift->platform, 
        //                    shift->arriving_hour, shift->arriving_min, shift->travel_time);
        //         }
        //     }
        // }
        temp = temp->next_neighbour;
    }
}