//  Original Author: Chris McDonald
//  linked list from cits2002 system programming

#ifndef LIST_H
#define LIST_H

#include "timetable.h"
#include "stdbool.h"

typedef struct list {
    char            *neighbour_name;
    char            *port;
    TIMETABLE       *timetable;
    struct list     *next_neighbour;
} LIST;

//  create a new linked list
extern LIST *list_init();

//  return true or false if the neighbour is in the list
extern bool list_find(LIST *neighbours, char *neighbour_name);

//  create new neighbour, use in list add
extern LIST *list_new_item(char *neighbour_name);

//  if neighbour in list, add new shift to neighbour->timetable
//  if not, add new neighbour
extern LIST *list_add(LIST *neighbours, char *neighbour_name, int leaving_hour, int leaving_min,
                int arriving_hour, int arriving_min, char *platform, char *line_name, char *next_station);

//  return localhost:prot_number
extern char *list_get_port(LIST *neighbours, char *neighbour_name);

//  return a struct SHIFT {leaving time, arriving time, trave time, platform, line name}
extern SHIFT *list_get_next_shift(LIST *neighbours, char *neighbour_name, int leaving_hour, int leaving_min);

//  print time table according to next station
extern void list_print_timetable(LIST *neighbours);

#endif /* LIST_H */