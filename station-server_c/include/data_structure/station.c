#include "station.h"

STATION *station_make(char *path, char *station_name, int browser_port, int query_port, int num_neighbour, char **neighbours_ports) {
    FILE *fp = fopen(path, "r");
    check_file_open(fp);

    char *line = malloc(LINE_SIZE);
    check_alloc(line);
    size_t buffer_size = LINE_SIZE;
    size_t bytes_read;

    STATION *station = malloc(sizeof(STATION));
    check_alloc(station);

    //  read first line, store station name and xy coordinates
    while ((bytes_read = getline(&line, &buffer_size, fp)) != -1) {
        if (line[0] == '#') {
            continue;
        }
        station->station_name_from_file = strtok(line, ",");
        station->x_coordinate = atof(strtok(NULL, ","));
        station->y_coordinate = atof(strtok(NULL, "\n"));
        break;
    }

    LIST *neighbours = list_init();
    //  read timetable, store data
    while ((bytes_read = getline(&line, &buffer_size, fp)) != -1) {
        if (line[0] == '#') {
            continue;
        }
        int leaving_hour                = atoi(strtok(line, ":"));  
        int leaving_min                 = atoi(strtok(NULL, ",")); 
        char *line_name                 = strtok(NULL, ",");
        char *platform                  = strtok(NULL, ",");
        int arriving_hour               = atoi(strtok(NULL, ":"));
        int arriving_min                = atoi(strtok(NULL, ","));
        char *next_station              = strtok(NULL, "\n");
        neighbours = list_add(neighbours, next_station, leaving_hour, leaving_min, arriving_hour, arriving_min, platform, line_name, next_station);
    }

    station->neighbours         = neighbours;
    station->station_name       = station_name;
    station->browser_port       = browser_port;
    station->query_port         = query_port;
    station->num_neighbour      = num_neighbour;
    station->neighbours_port    = neighbours_ports;

    free(line);
    fclose(fp);
    return station;
}

void station_print(STATION *station) 
{
    printf("station name: %s\t broswer port: %d\t query port: %d\n", station->station_name, station->browser_port, station->query_port);
    printf("x coordinate: %f\t y corrdinate: %f\n", station->x_coordinate, station->y_coordinate);
    printf("neighbour ports: ");
    for (int i = 0; i < station->num_neighbour; i++) {
        printf("%s, ", station->neighbours_port[i]);
    }
    printf("\n");
    list_print_timetable(station->neighbours);
}