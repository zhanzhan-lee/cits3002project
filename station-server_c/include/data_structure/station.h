#ifndef STATION_H
#define STATION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "list.h"
#include "../check/check.h"

typedef struct station {
    char                *station_name;
    int                 browser_port;
    int                 browser_socket;
    struct sockaddr_in  *browser_address;
    int                 query_port;
    int                 query_socket;
    struct sockaddr_in  *query_address;
    char                *station_name_from_file;
    float               x_coordinate;
    float               y_coordinate;
    int                 num_neighbour;
    char                **neighbours_port;
    LIST                *neighbours;
} STATION;

//  read timetable file and return a station struct
extern STATION *station_make(char *path, char *station_name, int browser_port, int query_port, int num_neighbour, char **neighbours_ports);

//  print station details
extern void station_print(STATION *station); 

#endif /* LIST_H */