#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "timetable.h"
#include "list.h"
#include "../check/check.h"

TIMETABLE *timetable_init()
{
    TIMETABLE *new_table = calloc(1, sizeof(TIMETABLE));
    check_alloc(new_table);
    // set all slot to NULL
    for (int i = 0; i < 25; i++) {
        for (int j = 0; j < 60; j++) {
            (*new_table)[i][j] = NULL;
        }
    }

    return new_table;
}

int timetable_travel_time(int leaving_hour, int leaving_min, int arriving_hour, int arriving_min)
{   
    if (arriving_hour < leaving_hour) { 
        arriving_hour += 24;
    }
    
    int start   = leaving_hour * 60 + leaving_min;
    int end     = arriving_hour * 60 + arriving_min; 

    int op      = end - start;
    return op;
}

void timetable_add(TIMETABLE *table, int leaving_hour, int leaving_min, int arriving_hour, int arriving_min, char *platform, char *line_name, char *next_station) 
{
    // create new shift
    SHIFT *new_shift = calloc(1, sizeof(SHIFT));
    check_alloc(new_shift);
    new_shift->leaving_hour     = leaving_hour;
    new_shift->leaving_min      = leaving_min;
    new_shift->arriving_hour    = arriving_hour;
    new_shift->arriving_min     = arriving_min;
    new_shift->travel_time      = timetable_travel_time(leaving_hour, leaving_min, arriving_hour, arriving_min);
    new_shift->platform         = strdup(platform);
    check_alloc(new_shift->platform);
    new_shift->line_name        = strdup(line_name);
    check_alloc(new_shift->line_name);
    new_shift->next_station     = strdup(next_station);
    check_alloc(new_shift->next_station);
    // new_shift->next_shift       = NULL;

    // add new shift to table
    (*table)[leaving_hour][leaving_min] = new_shift;
}

SHIFT *timetable_find_next(TIMETABLE *table, int leaving_hour, int leaving_min)
{
    while ((*table)[leaving_hour][leaving_min] == NULL) {
        leaving_min++;

        if (leaving_min > 59) {
            leaving_min = 0;
            leaving_hour++;
        }
    }

    return (*table)[leaving_hour][leaving_min];
}

void timetable_free(TIMETABLE *table)
{
    for (int i = 0; i < 24; i++) {
        for (int j = 0; j < 60; j++) {
            free((*table)[i][j]);
        }
    }
    free(table);
}
