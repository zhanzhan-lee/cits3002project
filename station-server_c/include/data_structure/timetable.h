#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <ctype.h>

typedef struct shift {
    int     leaving_hour;
    int     leaving_min;
    int     arriving_hour;
    int     arriving_min;
    int     travel_time;
    char    *line_name;
    char    *platform;
    char    *next_station;
    // SHIFT   *next_shift;
} SHIFT;

#define LINE_SIZE 100

typedef SHIFT *TIMETABLE[25][60];

extern __ssize_t getline();
extern char *strdup();

//  create new timetable
extern TIMETABLE *timetable_init();

//  calculate travel time to next neighbour
extern int timetable_travel_time(int leaving_hour, int leaving_min, int arriving_hour, int arriving_min);

//  add new shift to timetable
extern void timetable_add(TIMETABLE *table, int leaving_hour, int leaving_min, int arriving_hour, int arriving_min, char *platform, char *line_name, char *next_station);

//  find next shift
extern SHIFT *timetable_find_next(TIMETABLE *table, int leaving_hour, int leaving_min);

//  free timetable
extern void timetable_free(TIMETABLE *table);

#endif /* TIMETABLE_H */
