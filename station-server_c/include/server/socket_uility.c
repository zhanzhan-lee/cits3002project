#include "socket_uility.h"

int socket_init(int socket_type)
{
    int sd;

    switch (socket_type) {
        case 0: 
            sd = socket(AF_INET, SOCK_STREAM, 0);
            break;
        case 1:
            sd = socket(AF_INET, SOCK_DGRAM, 0);
            break;
    }

    check(sd, "socket init");
    return sd;
}

struct sockaddr_in *socket_create_address(char *ip, int port)
{
    struct sockaddr_in *address  = malloc(sizeof(struct sockaddr_in));
    check_alloc(address);
    address->sin_family          = AF_INET;
    address->sin_port            = htons(port);
    inet_pton(AF_INET, ip, &address->sin_addr.s_addr);
    return address;
}