#ifndef SOCKET_UILITY_H
#define SOCKET_UILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h> 

#include "../check/check.h"

//  create TCP/IPv4 socket
extern int socket_init(int socket_type);

//  create IPv4 address
extern struct sockaddr_in *socket_create_address(char *ip, int port);

#endif /* SOCKET_UILITY */