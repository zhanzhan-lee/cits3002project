#include "tcp.h"

REQUEST *tcp_request_handle(int client_socket)
{
    char *buffer = malloc(sizeof(char) * 1024);
    check_alloc(buffer);

    REQUEST *request = malloc(sizeof(REQUEST));
    ssize_t bytes_received = recv(client_socket, buffer, 1024, 0);
    if (bytes_received < 0) {
        perror("recv");
        request->type = 2;
        return request;
    }

    if (strncmp(buffer, "GET /?hour=", 11) == 0) {
        request->type = 1;
        strtok(buffer, "=");
        request->hour = atoi(strtok(NULL, "&"));
        strtok(NULL, "=");
        request->min = atoi(strtok(NULL, "&"));
        strtok(NULL, "=");
        request->source = strtok(NULL, "&");
        strtok(NULL, "=");
        request->destination = strtok(NULL, " ");
        return request;
    }

    if (strncmp(buffer, "GET / HTTP/", 10) == 0) {
        request->type = 0;
        return request;
    }

}

long tcp_count_HTML(FILE *fd) 
{
    fseek(fd, 0, SEEK_END);
    long page_size = ftell(fd);
    fseek(fd, 0, SEEK_SET);

    return page_size;
}

char *tcp_read_HTML(FILE *fd, long size) 
{
    char *page = malloc(size + 1);
    check_alloc(page);

    fread(page, 1, size, fd);
    page[size] = '\0';

    fclose(fd);
    return page;
}

void tcp_serve_HTML(int client_socket)
{
    FILE *fd = fopen("../HTML/index.html", "r");
    check_file_open(fd);
    long page_size = tcp_count_HTML(fd);
    char *page = tcp_read_HTML(fd, page_size);

    const char *response_header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: %ld\r\n\r\n%s";
    int response_size = snprintf(NULL, 0, response_header, page_size, page);
    char *response = malloc(response_size + 1);
    check_alloc(response);

    snprintf(response, response_size + 1, response_header, page_size, page);
    send(client_socket, response, response_size, 0);

    free(page);
    free(response);
    close(client_socket);
}

ROUTE *tcp_find_destination(STATION *station, REQUEST *request)
{
    LIST *neighbours = station->neighbours;

    ROUTE *route = malloc(sizeof(ROUTE));
    check_alloc(route);

    while (neighbours != NULL) {
        if (strcmp(request->destination, neighbours->neighbour_name) == 0) {
            SHIFT *new_shift = malloc(sizeof(SHIFT));

            new_shift = timetable_find_next(neighbours->timetable, request->hour, request->min);
            route->shift = new_shift;
            route->next_shift = NULL;
            return route;
        }
        neighbours = neighbours->next_neighbour;
    }

    return NULL;
}

void tcp_send_HTML(int client_socket, REQUEST *request, ROUTE *route) 
{
    //  upper part of the page
    FILE *fd_upper = fopen("../HTML/index_upper.html", "r");
    check_file_open(fd_upper);
    size_t page_size_upper = tcp_count_HTML(fd_upper);
    char *page_upper = tcp_read_HTML(fd_upper, page_size_upper);

    //  lower part of the page
    FILE *fd_lower = fopen("../HTML/index_lower.html", "r");
    check_file_open(fd_lower);
    size_t page_size_lower = tcp_count_HTML(fd_lower);
    char *page_lower = tcp_read_HTML(fd_lower, page_size_lower);

//  TODO get the route   ------------------------------------------------
    const char *response_answer = 
                "<div class=\"box\">\n"
                "<h1>Route to destination</h1>\n"
                "source: %s<br>"
                "line: %s platform: %s leaving time: %02d:%02d next station: %s arriving time: %02d:%02d<br>"      //  make a loop return an linked list?
                "destination: %s\tarriving time: <br>"
                "</div>\n";

    size_t response_answer_size = snprintf(NULL, 0, response_answer, request->source, route->shift->line_name, route->shift->platform,
                                            route->shift->leaving_hour, route->shift->leaving_min, route->shift->next_station, 
                                            route->shift->arriving_hour, route->shift->arriving_min, request->destination);

//  ---------------------------------------------------------------------

    const char *response_header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: %ld\r\n\r\n";
    size_t response_header_size = snprintf(NULL, 0, response_header, page_size_upper + page_size_lower + response_answer_size);
    
    //  combine header, upper, answer, lower
    size_t total_size = response_header_size + page_size_upper + page_size_lower + response_answer_size + 1;
    char *response = malloc(total_size);
    check_alloc(response);

    snprintf(response, total_size, response_header, page_size_upper + page_size_lower + response_answer_size);
 
    strcat(response, page_upper);
    snprintf(response + strlen(response), response_answer_size + 1, response_answer, request->source, route->shift->line_name, 
                route->shift->platform, route->shift->leaving_hour, route->shift->leaving_min, route->shift->next_station, 
                route->shift->arriving_hour, route->shift->arriving_min, request->destination);
 
    strcat(response, page_lower);

    printf("Serve response HTML\n");
    send(client_socket, response, response_header_size + page_size_upper + page_size_lower + response_answer_size, 0);

    free(page_upper);
    free(page_lower);
    free(response);
    close(client_socket);
}

void *tcp_listen(void *args)
{
    STATION *station = (STATION *)args;
    // int browser_socket                       = socket_init(0);
    // struct sockaddr_in browser_address       = *socket_create_address(IP, station->browser_port);

    check(bind(station->browser_socket, (struct sockaddr *)&station->browser_address, sizeof(station->browser_address)), "bind");
    check(listen(station->browser_socket, 1), "bind");

    printf("browser port ready: %d\n", station->browser_port);
    
    while (1) {
        struct sockaddr_in *client_address  = malloc(sizeof(struct sockaddr_in));
        socklen_t client_address_size       = sizeof(client_address);
        int client_socket;
        check(client_socket = accept(station->browser_socket, (struct sockaddr *)&client_address, &client_address_size), "accept");
 
        printf("client connected\n");
        REQUEST *request = tcp_request_handle(client_socket);
        switch (request->type) {
            case 0:
                printf("serve HTML\n");
                tcp_serve_HTML(client_socket);
                break;

            case 1:
                printf("GET request received, find destination...\n");
                printf("source: %s\tdest: %s\tstarting time: %02d:%02d\n", request->source, request->destination, request->hour, request->min);
                ROUTE *route = tcp_find_destination(station, request);
                if (route != NULL) {
                    tcp_send_HTML(client_socket, request, route);
                } else {
                    printf("not dircetly connected to destination, asking neighbours\n");
                    // char *message = "Hello, UDP server!";
                    // check(sendto(query_socket, (const char *)message, strlen(message), 0, (const struct sockaddr *)&query_address, sizeof(struct sockaddr)), "server to udp"); 
                }
                break;

            case 2:
                printf("continue listening\n");
                break;
        } 
    }

    close(station->browser_socket);
}

// Extract the value of the "to" parameter from a GET request
// char *server_GET_handle2(int client_socket) {
//     static char to_value[256]; // Static buffer to hold the "to" parameter value
//     memset(to_value, 0, sizeof(to_value)); // Clear the buffer

//     char request_buffer[1024];
//     ssize_t bytes_received = recv(client_socket, request_buffer, sizeof(request_buffer), 0);
//     if (bytes_received < 0) {
//         perror("error receiving");
//         return NULL;
//     }

//     request_buffer[bytes_received] = '\0';  // Null-terminate the received data

//     // Extract the URL part of the request
//     char *url_start = strstr(request_buffer, "GET ") + 4;
//     char *url_end = strchr(url_start, ' ');
//     if (url_end) {
//         *url_end = '\0';  // Null-terminate the URL
//     }

//     // Parse the "to" parameter from the query string
//     char *query_start = strchr(url_start, '?');
//     if (query_start) {
//         query_start++; // Skip the '?'
//         char *param_start = strstr(query_start, "to=");
//         if (param_start) {
//             param_start += 3; // Skip "to="
//             char *param_end = strchr(param_start, '&');  // Find the end of the parameter
//             if (!param_end) {
//                 param_end = url_end;  // Use the end of the URL if there's no other parameter
//             }
//             if (param_end) {
//                 size_t length = param_end - param_start;
//                 if (length < sizeof(to_value)) {
//                     strncpy(to_value, param_start, length);
//                     to_value[length] = '\0';  // Ensure null termination
//                 }
//             }
//         }
//     }

//     return to_value;  // Return the value of the "to" parameter or an empty string if not found
// }

// void send_json_response(int client_socket) {
//     const char *json = 
//         "{\n"
//         "  \"destination\": \"TerminalA\",\n"
//         "  \"nextDeparture\": \"19:00\",\n"
//         "  \"from\": \"JunctionB\"\n"
//         "}\n";

//     char response[1024];
//     snprintf(response, sizeof(response), 
//         "HTTP/1.1 200 OK\r\n"
//         "Content-Type: application/json; charset=UTF-8\r\n"
//         "Content-Length: %lu\r\n"
//         "\r\n"
//         "%s", strlen(json), json);

//     if (write(client_socket, response, strlen(response)) < 0) {
//         perror("Failed to send response");
//     }
// }

// void send_HTML_response(int client_socket) {
//     const char *header =
//         "HTTP/1.1 200 OK\r\n"
//         "Content-Type: text/html; charset=UTF-8\r\n\r\n";
//     const char *htmlContent =
//         "<html>\n"
//         "<head><title>Your Route</title></head>\n"
//         "<body style=\"background-image: url('https://images.assetsdelivery.com/compings_v2/zoaarts/zoaarts1810/zoaarts181000012.jpg'); background-repeat: repeat;\">\n"
//         "<h1>Route Information</h1>\n"
//         "<p>Your selected destination: TerminalA</p>\n"
//         "<p>Next departure: 19:00 from JunctionB</p>\n"
//         "</body>\n"
//         "</html>\n";

//     char response[1024];
//     int length = snprintf(response, sizeof(response), "%s%s", header, htmlContent);
    
//     if (send(client_socket, response, length, 0) < 0) {
//         perror("Failed to send response");
//     }

//     close(client_socket);
// }


