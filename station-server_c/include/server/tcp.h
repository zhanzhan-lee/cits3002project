#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "socket_uility.h"
#include "../check/check.h"
#include "../data_structure/station.h"

#define IP  "127.0.0.1"

typedef struct REQUEST {
    int     type;
    char    *source;
    char    *destination;
    int     hour;
    int     min;
} REQUEST;

//  TODO figure out how to present the answer

typedef struct ROUTE {
    SHIFT   *shift;
    SHIFT   *next_shift;
} ROUTE;

//  handle incoming request
extern REQUEST *tcp_request_handle(int client_socket);

long tcp_count_HTML(FILE *fd);

char *tcp_read_HTML(FILE *fd, long size);

extern ROUTE *tcp_find_destination(STATION *station, REQUEST *requset);

extern void tcp_serve_HTML(int client_socket);

extern void tcp_send_HTML(int client_socket, REQUEST *request, ROUTE *route);

extern char *server_GET_handle2(int client_socket);

extern void send_json_response(int client_socket);

extern void send_HTML_response(int client_socket);

//  initiate server listening on browser port
extern void *tcp_listen(void *args);

#endif /* SERVER_H */