#include "udp.h"
#include "socket_uility.h"

#define MAX_CLIENTS 10

void *udp_listen(void * args) {
    STATION *station = (STATION *)args;
    // int server_socket = socket_init(1);
    // struct sockaddr_in server_address = *socket_create_address(IP, station->query_port);

    check(bind(station->query_socket, (struct sockaddr *)&station->query_address, sizeof(station->query_address)), "udp bind");
    printf("query port ready: %d\n", station->query_port);

    fd_set read_set;
    char buffer[1024];
    struct sockaddr_in client_address[MAX_CLIENTS];
    socklen_t client_address_size = sizeof(struct sockaddr_in);

    FD_ZERO(&read_set);
    FD_SET(station->query_socket, &read_set);
    int max_sd = station->query_socket;

    while (1) {
        fd_set temp_fds = read_set;

        if (select(max_sd + 1, &temp_fds, NULL, NULL, NULL) >= 0) {
            if (FD_ISSET(station->query_socket, &temp_fds)) {
                ssize_t bytes_received;
                check(bytes_received = recvfrom(station->query_socket, buffer, sizeof(buffer), 0, (struct sockaddr *)&client_address[0], &client_address_size), "recfrom");
                buffer[bytes_received] = '\0';
                printf("Received message from %s:%d: %s\n", inet_ntoa(client_address[0].sin_addr), ntohs(client_address[0].sin_port), buffer);
            }
        }
    }

    close(station->query_socket);
}