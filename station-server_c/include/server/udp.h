#ifndef UDP_H
#define UDP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>

#include "socket_uility.h"
#include "../check/check.h"
#include "../data_structure/station.h"

#define IP  "127.0.0.1"
#define MAX_CLIENTS 10

//  initiate server listening on browser port
extern void *udp_listen(void *args);

#endif /* SERVER_H */