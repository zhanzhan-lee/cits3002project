#include "station-server.h"

void usage()
{
    printf("usage: /station [station_name] [browser-port] [query-port] [neighbour......]\n");
    exit(EXIT_SUCCESS);
}

//  ./station station_name browser-port query-port neighbour......
int main(int argc, char *argv[]) 
{
    if (argc < 5) {
        usage();
    }

    char *station_name      = argv[1];
    int browser_port        = atoi(argv[2]);
    int query_port          = atoi(argv[3]);
    int num_neighbours      = argc - 4;

    char **neighbours_ports = malloc(num_neighbours * sizeof(char*));
    check_alloc(neighbours_ports);

    for (int i = 0; i < num_neighbours; i++) {
        neighbours_ports[i] = argv[4 + i];
    }

    char *path = malloc(strlen(station_name) + sizeof(char) * 23);
    check_alloc(path);
    snprintf(path, strlen(station_name) + sizeof(char) * 23, "../data_timetable/tt-%s", station_name);
          
    STATION *station        = station_make(path, station_name, browser_port, query_port, num_neighbours, neighbours_ports);
    station_print(station);

    int browser_socket                       = socket_init(0);
    struct sockaddr_in browser_address       = *socket_create_address(IP, browser_port);

    int query_socket                         = socket_init(1);
    struct sockaddr_in query_address         = *socket_create_address(IP, query_port);

    station->browser_socket                  = browser_port;
    station->browser_address                 = malloc(sizeof(struct sockaddr_in));
    station->browser_address                 = &browser_address;   

    station->query_socket                    = query_port;
    station->query_address                   = malloc(sizeof(struct sockaddr_in));
    station->query_address                   = &query_address;

    pthread_t browser_thread, query_thread;

    check(pthread_create(&browser_thread, NULL, tcp_listen, (void *)station), "brower thread");
    check(pthread_create(&query_thread, NULL, udp_listen, (void *)station), "query thread");

    pthread_join(browser_thread, NULL);
    pthread_join(query_thread, NULL);

    return 0;
}