#ifndef STATIONS_H
#define STATIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>

#include "include/check/check.h"
#include "include/data_structure/timetable.h"
#include "include/data_structure/list.h"
#include "include/data_structure/station.h"
#include "include/server/tcp.h"
#include "include/server/udp.h"

#endif /* STATIONS_H */