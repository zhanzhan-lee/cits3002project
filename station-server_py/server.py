import socket

def handle_request(data):
    """解析请求并生成响应"""
    headers = data.split('\n')
    request_type = headers[0].split()[0]
    print(f"Received {request_type} request")
    
    if request_type == 'GET':
        return "HTTP/1.1 200 OK\n\nHello from GET!"
    elif request_type == 'POST':
        return "HTTP/1.1 200 OK\n\nHello from POST!"
    else:
        return "HTTP/1.1 400 Bad Request\n\nUnknown request type."

def main():
    host = 'localhost'
    port = 4444
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind((host, port))
        server_socket.listen(5)
        print(f"Listening on {host}:{port}...")

        while True:
            client_connection, client_address = server_socket.accept()
            with client_connection:
                request = client_connection.recv(1024).decode('utf-8')
                print(f"Received request: {request}")
                response = handle_request(request)
                client_connection.sendall(response.encode('utf-8'))

if __name__ == '__main__':
    main()

#ssh-keygen -t rsa -b 4096 -C "zhanzhan12821@gmail.com"
